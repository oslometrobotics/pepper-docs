# Hioa Pepper #

The offical user guide: [User guide (link)](http://doc.aldebaran.com/2-5/family/pepper_user_guide/interacting.html)

### Simple Use Guide pepper ###

**Turning on:**  
Press the button on the center of its chest (localted under the tablet).

**Turning off:**  
Press the button on the chest and hold for 4 seconds.

**Put pepper in sleep mode:**  
There are a couple of ways of doing this:
Press the chest button two times.
Say to pepper: "Pepper go to sleep".
Hold your hand over the camera in peppers forhead for a couple of seconds.

**Wake pepper**  
Stroke the top of of the head to wake pepper (there are two sensor bars of top of the head)  

### Pepper info and administration ###

Password:
YwQDXvPEno

**Pepper's email**
Email: hioa.pepper@gmail.com  
Same password as above.

**Connect via browser**
URL: "ip.address"
Same password as above.

**Connect via SSH**
ssh nao@”ip.address”
Same password as above.

**Connect via FTP**
- host: "ip.address"
- un:   nao
- pw:   same as above
- port: 22

#### Pepper calander ####

[Pepper Calender](https://teamup.com/ksjqa7vr2tjf4ivw6u)  
Read link:  
https://teamup.com/ksjqa7vr2tjf4ivw6u  
Modify link:  
https://teamup.com/ksbqhkowmp9f4emmex  
Admin (Careful):  
https://teamup.com/ks31e9bhnxojbq5jg7  

**Old pepper folder (google drive)**  
[Pepper drive](https://drive.google.com/drive/folders/0B78b1sfVV8XzdVBjYldrb3pQc00?usp=sharing)

**Shared work log**
[Google drive](https://docs.google.com/document/d/1X_LJrqbP4IYK6QYfZS0mJV_DbC4NYMmT2lzFhV8hmFs/edit?usp=sharing)

### Contact ###

#### Administration #####

Alex: Alex.Alcocer@hioa.no

#### Student Contact ####

Vako: vakonairy@gmail.com  
Jonas: egecarlsen@gmail.com
Åsmund: asmundwien@gmail.com

### Usefull stuff ###
Peppers apps are located at dir
/data/home/nao/.local/share/PackageManager/apps/
