"""
Pepper says hi to people around her
"""

import time
import sys
import argparse
import qi
from random import randint

class Triggers(object):

    def __init__(self, app):
        super(Triggers, self).__init__()
        app.start()
        session = app.session
        self.memory = session.service("ALMemory")
        self.tts = session.service("ALTextToSpeech")

        # People
        self.justArrived = self.memory.subscriber("PeoplePerception/JustArrived")
        self.justArrived.signal.connect(self.onJustArrived)

        self.justLeft = self.memory.subscriber("PeoplePerception/JustLeft")
        self.justLeft.signal.connect(self.onJustLeft)

        self.youSmiling = self.memory.subscriber("FaceCharacteristics/PersonSmiling")
        self.youSmiling.signal.connect(self.onYouSmiling)

    def onJustArrived(self, value):
        greetings = ["Hi there", "Hi", "Hey", "Hello", "Howdy", "Greetings", "Greetings human"]
        self.tts.say(greetings[randint(0, 6)])
        print "Person arrived: %s" % (value)

    def onJustLeft(self, value):
        self.tts.say("Gone")
    
    def onYouSmiling(self, value):
        greetings = ["Such a nice smile", "I'm glad you're happy", "I love that smile", "You're teeth are so white", "I'm happy you're happy"]
        self.tts.say(greetings[randint(0, 4)])
        print value

    def run(self):
        print "Starting Triggers"
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            print "Interrupted by user, stopping Triggers"
            #stop
            sys.exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.137.245",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    args = parser.parse_args()
    try:
        # Initialize qi framework.
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["Triggers", "--qi-url=" + connection_url])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    triggers = Triggers(app)
    triggers.run()
    