from msvcrt import getch
from random import randint
import sys
import argparse
import qi


def main(session):
    greetings = ["Hi there", "Hi", "Hey", "Hello", "Howdy", "Greetings", "Greetings human"]

    tts = session.service("ALTextToSpeech")

    while True:
        key = getch()
        if key == " ":
            print greetings[randint(0, 6)]
            tts.say(greetings[randint(0, 6)])

        elif key == "q":
            sys.exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.137.160",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) + ".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    main(session)
