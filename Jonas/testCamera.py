import qi
import sys
import time
import argparse

def main(session):

    Photo = session.service("ALPhotoCapture")
    tts = session.service("ALTextToSpeech")
    tabletService = session.service("ALTabletService")
    audioPlayer = session.service("ALAudioPlayer")

    fileId = audioPlayer.loadFile("/home/nao/.local/share/PackageManager/apps/imageStore/html/sounds/shutter.wav")

    time.sleep(5)
    Photo.setResolution(3)
    Photo.setPictureFormat("jpg")
    tts.say("3")
    time.sleep(1)
    tts.say("2")
    time.sleep(1)
    tts.say("1")
    time.sleep(1)
    audioPlayer.play(fileId)
    Photo.takePictures(1, "/home/nao/.local/share/PackageManager/apps/imageStore/html/img", "selfie")
    time.sleep(3)
    tabletService.showWebview("http://198.18.0.1/apps/imageStore/selfie.html")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="172.20.10.3",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    main(session)