'''



'''
import time
import sys
import argparse
import qi

class Triggers(object):

    def __init__(self, app):
        super(Triggers, self).__init__()
        app.start()
        session = app.session

        # Creating a proxy with the Memory service
        self.memory = session.service("ALMemory")

        #
    def printLaserValue(self):
        print "Laser: %.3f" % (self.memory.getData("Device/SubDeviceList/Platform/LaserSensor/Front/Horizontal/Seg02/X/Sensor/Value"))

    def printIRValue(self):
       print "IR: %.3f" % (self.memory.getData("Device/SubDeviceList/Platform/InfraredSpot/Left/Sensor/Value"))
    
    def run(self):
        print "Starting Triggers"
        try:
            while True:
                time.sleep(1)
                trigger.printLaserValue()
                trigger.printIRValue()
        except KeyboardInterrupt:
            print "Interrupted by user, stopping Triggers"
            #stop
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.137.4",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    args = parser.parse_args()
    try:
        # Initialize qi framework.
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["Triggers", "--qi-url=" + connection_url])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    trigger = Triggers(app)
    trigger.run()