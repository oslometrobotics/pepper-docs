import qi
import time
import sys
import argparse
import math

def main():
	# Creating proxy
    memory_service = session.service("ALMemory")
    motion_service = session.service("ALMotion")
    tracker_service = session.service("ALTracker")
    # Setting a target name, and face with of the target, average human face is about 10-12 cm.
    targetName = "Target"
    faceWidth = 0.10
    # We save the target and track it
    tracker_service.registerTarget(targetName, faceWidth)
    tracker_service.track(targetName)
    # Wait 3 seconds
    time.sleep(3)
    # Run boduTurn
    bodyTurn()


def bodyTurn():
    print "turning body " + str(memory_service.getData("Device/SubDeviceList/HeadYaw/Position/Sensor/Value")) + "rad"
    tracker_service.lookAt(targetName)
    motion_service.moveInit()
    motion_service.moveTo(0,0,memory_service.getData("Device/SubDeviceList/HeadYaw/Position/Sensor/Value"), _async=True)
    tracker_service.lookAt(targetName)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.137.249",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    args = parser.parse_args()
    try:
        # Initialize qi framework.
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["Explore", "--qi-url=" + connection_url])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
main()