import sys
import argparse
import qi
import math
import time
 
 
 
class autonomous(object):
 
    alreadyMoving = False
 
    def __init__(self, app):
        super(autonomous, self).__init__()
        app.start()
        session = app.session
 
        self.memory = session.service("ALMemory")
        self.tts = session.service("ALTextToSpeech")
        self.motion = session.service("ALMotion")
        self.posture = session.service("ALRobotPosture")
        #left bumper
        self.leftBumper = self.memory.subscriber("LeftBumperPressed")
        self.leftBumper.signal.connect(self.onBumper)
        #right bumper
        self.rightBumper = self.memory.subscriber("RightBumperPressed")
        self.rightBumper.signal.connect(self.onBumper)
        #back bumper
        self.backBumper = self.memory.subscriber("BackBumperPressed")
        self.backBumper.signal.connect(self.onBumper)
 
    def onBumper(self, value):
        #bumper trigger
        if value == 1:
            self.onStop()     
 
    def onStop(self):
        #pepper stops and awaits input
        self.motion.moveInit()
        self.motion.move(0,0,0)
 
 
    def onTurn(self, direction):
        #Turn logic

        if direction == "right":
            self.motion.moveTo(0, 0, -(math.pi)/2)
            print "snur meg til hoyre"

        elif direction == "left":
            print "snur meg til venstre"
            self.motion.moveTo(0, 0, (math.pi)/2)

        elif direction == "back":
            print "rygger"
            self.motion.moveTo(-1, 0, 0)

        elif direction == "front":

            if self.memory.getData("Device/SubDeviceList/Platform/InfraredSpot/Right/Sensor/Value") and self.memory.getData("Device/SubDeviceList/Platform/InfraredSpot/Left/Sensor/Value") == 1:
                print "rygger"
                self.turn("back")
                # move back 1 meter 

            if self.memory.getData("Device/SubDeviceList/Platform/InfraredSpot/Left/Sensor/Value") == 1:
                print "noe til venstre, maa snu meg"
                self.turn("left")

            elif self.memory.getData("Device/SubDeviceList/Platform/InfraredSpot/Right/Sensor/Value") == 1:
                print "noe til hoyre, maa snu meg"
                self.turn("right")

    def Move(self):
        self.scriptRunning = True
        self.posture.goToPosture("StandInit", 2)
        self.motion.waitUntilMoveIsFinished()
        try:
            while self.scriptRunning == True:
                print "moving forwards"
                self.motion.moveInit()
                self.motion.move(0.25, 0, 0)
 
                if self.memory_service.getData("Device/SubDeviceList/Platform/Front/Sonar/Sensor/Value") > 0.5: 
                    #hvis sonaren framme oppdager noe innen 0.5m rekkevidde for roboten
                    self.onTurn("front")

        except KeyboardInterrupt:
            print "Stopping"
            self.scriptRunning = False
            self.onStop()
            sys.exit(0)
                     
 
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.137.204",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")
 
    args = parser.parse_args()
    try:
        # Initialize qi framework.
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["MoveTurn", "--qi-url=" + connection_url])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
 
    autono = autonomous(app)
    autono.Move()