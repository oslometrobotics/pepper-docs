import qi
import sys
import time
import argparse
from datetime import datetime
 
class Triggers(object):
 
    def __init__(self, app):
        super(Triggers, self).__init__()
        app.start()
        session = app.session

        # Creating proxy
        self.memory = session.service("ALMemory")

        # Getting Front/Sonar/Sensor/Value
    def onSonarFront(self):
        self.now = datetime.now()
        print '%s:%s' % (self.now.minute, self.now.second) + str(self.memory.getData("Device/SubDeviceList/Platform/Front/Sonar/Sensor/Value"))

        # Running onSonarFront 4 times a second. Ctrl + C stops the script
    def run(self):
        print "Starting Triggers"
           
        try:
            while True:
                triggers.onSonarFront()
                time.sleep(0.25)
        except KeyboardInterrupt:
            print "Interrupted by user, stopping Triggers"
            sys.exit(0)
 
 
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.137.84",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")
 
    args = parser.parse_args()
    try:
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["Triggers", "--qi-url=" + connection_url])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
 
    triggers = Triggers(app)
    triggers.run()