"""
Stalker function for Softbank Robotics Pepper robot.

Author Vako Varnkian in collaboration with OsloMet Robotics and Bachelor Thesis Group 1821 

20/5/2018

"""
# Importing libraries
import time
import sys
import argparse
import qi
import math

class Explore(object):
    """Main class"""

    def __init__(self, app):
        super(Explore, self).__init__()

        app.start()
        session = app.session

        # Creating proxies to the different APIs
        self.memory_service = session.service("ALMemory")
        self.motion_service = session.service("ALMotion")
        self.autonomous = session.service("ALAutonomousLife")
        self.tracker_service = session.service("ALTracker")
        self.posture = session.service("ALRobotPosture")
        self.listen = session.service("ALListeningMovement")
        self.speak = session.service("ALSpeakingMovement")
        self.backround = session.service("ALBackgroundMovement")
        self.basic = session.service("ALBasicAwareness")

        # Subscribing to the event
        self.leftBumper = self.memory_service.subscriber("LeftBumperPressed")
        self.rightBumper = self.memory_service.subscriber("RightBumperPressed")        
        self.backBumper = self.memory_service.subscriber("BackBumperPressed")
        # Connecting the event to a function
        self.leftBumper.signal.connect(self.onBumper)
        self.rightBumper.signal.connect(self.onBumper)
        self.backBumper.signal.connect(self.onBumper)

        # The robot gets in position to start the script.
        self.posture.goToPosture("StandInit", 2)

        # Disabeling some of Peppers basic features.
        self.listen.setEnabled(False)
        self.speak.setEnabled(False)
        self.backround.setEnabled(False)
        self.basic.setEnabled(True)
        self.basic.setTrackingMode("Head")
        self.basic.setStimulusDetectionEnabled("Sound", False)
        self.basic.setStimulusDetectionEnabled("TabletTouch", False)
        self.basic.setStimulusDetectionEnabled("Touch", False)

        # Disabling safety barriers
        self.autonomous.setSafeguardEnabled("RobotPushed", False)
        self.autonomous.setSafeguardEnabled("RobotMoved", False)
        self.autonomous.setSafeguardEnabled("RobotFell", False)

    def onBumper(self, value):
        """Bumper event trigger reaction"""
        if value == 1:
            self.onStop()

    def motionRoll(self):
    	# Function for rolling forwards
        print "robot is supposed to move | motionRoll"  
        self.motion_service.moveInit()
        self.motion_service.move(0.175,0,0, _async=True)

    def onStop(self):
    	# Function for stop
        self.motion_service.moveInit()
        self.motion_service.move(0,0,0, _async=True)

    def bodyTurn(self):
    	# Reads HeadYaw data and rotates relatively to where Pepper is looking
        print "turning body"
        self.motion_service.moveInit()
        self.motion_service.moveTo(0,0,self.memory_service.getData("Device/SubDeviceList/HeadYaw/Position/Sensor/Value"), _async=True)

    def onTurn(self, direction):
        # Turn logic
        # Pepper turns the oppisite side of the obstacle, stops if the obstacle is in front of it
        if direction == "right": 
            self.motion_service.moveInit()
            self.motion_service.moveTo(0, 0, -(math.pi)/2, _async=True)
            print "onTurn Dir: Right"

        elif direction == "left":
            print "onTurn Dir: Left"
            self.motion_service.moveInit()
            self.motion_service.moveTo(0, 0, (math.pi)/2, _async=True)

        elif direction == "back": 
        	# Pepper is trapped and needs to turn around
            print "Backing up"
            self.motion_service.moveTo(0, 0, math.pi, _async=True)
            self.motion_service.waitUntilMoveIsFinished()
        return

    def run(self):
        # This function will keep looping, until keyboard interuptet (Ctrl + C)
        print "Script started"
        self.scriptRunning = True

        # Parameters for the tracker service
        targetName = "Target"
        faceWidth = 0.1
        # Save the target
        self.tracker_service.registerTarget(targetName, faceWidth)
        # Do this untill keyboard interupt occurs
        try:
            while self.scriptRunning == True:
            	# move forwards if nothing is nearby
                if ( self.memory_service.getData("Device/SubDeviceList/Platform/Front/Sonar/Sensor/Value") > 0.5 and self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Left/Sensor/Value") == 0 and self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Right/Sensor/Value")== 0):
                    # Track and start moving, we track again here to always track before moving
                    self.tracker_service.track(targetName)
                    self.motionRoll()


                    

                    if (self.memory_service.getData("Device/SubDeviceList/HeadYaw/Position/Sensor/Value") > 0.45 or self.memory_service.getData("Device/SubDeviceList/HeadYaw/Position/Sensor/Value") < -0.45):
                        self.bodyTurn()

                    print "Front Sonar Value: " + str(self.memory_service.getData("Device/SubDeviceList/Platform/Front/Sonar/Sensor/Value"))
                elif self.memory_service.getData("Device/SubDeviceList/Platform/Front/Sonar/Sensor/Value") < 0.5:
                	self.onStop()

                elif self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Left/Sensor/Value") == 1:
                    print "InfraredLeft: " + str(self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Left/Sensor/Value"))
                    self.onTurn("right")

                elif self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Right/Sensor/Value") == 1:
                    print "InfraredRight: " + str(self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Right/Sensor/Value"))
                    self.onTurn("left")

                    # When Both IR sensors trigger and the sonar sensor show a low value, Pepper turns around
                elif (self.memory_service.getData("Device/SubDeviceList/Platform/Front/Sonar/Sensor/Value") < 0.5) and (self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Right/Sensor/Value") and self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Left/Sensor/Value") == 1):
                	print "InfraredRight: " + str(self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Right/Sensor/Value"))
                	print "InfraredLeft: " + str(self.memory_service.getData("Device/SubDeviceList/Platform/InfraredSpot/Left/Sensor/Value"))
                	self.onTurn("back")

            print "Exit script"
        except KeyboardInterrupt:
            print "Stopping"
            self.scriptRunning = False
            self.onStop()
            sys.exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.8.100",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    args = parser.parse_args()
    try:
        # Initialize qi framework.
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["Explore", "--qi-url=" + connection_url])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    goex = Explore(app)
    goex.run()