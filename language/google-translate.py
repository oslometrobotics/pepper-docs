""" -- Watson translate. Worsk, but can't translate Norwegian. --

import json
from watson_developer_cloud import LanguageTranslatorV2 as LanguageTranslator

language_translator = LanguageTranslator(
    username='accb23e5-6c3b-4bcb-9ebe-7e7aa97ecefb',
    password='0VWBBx8HnOXy')

translation = language_translator.translate(
    text='Hello Vako. You are a very nice person',
    model_id='en-es')
print(json.dumps(translation, indent=2, ensure_ascii=False))"""


""" -- Google translate which bypasses Google Cloud, and gets ajax data directly from translate.google.com -- """
from googletrans import Translator
translator = Translator()
translations = translator.translate(['The quick brown fox', 'jumps over', 'the lazy dog'], dest='no')
for translation in translations:
    print(translation.origin, ' -> ', translation.text)

print translator.translate(["This is text and I like it"], dest="no")[0].text
